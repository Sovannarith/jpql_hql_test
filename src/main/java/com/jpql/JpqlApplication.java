package com.jpql;

import com.jpql.entity.Employee;
import com.jpql.repository.empRepository;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.w3c.dom.Document;

import javax.persistence.*;
import java.util.List;

@SpringBootApplication
public class JpqlApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(JpqlApplication.class, args);
	}

	@Autowired
	empRepository empRepository;

	@Override
	public void run(String... strings) throws Exception {
//		System.out.println(empRepository.deleteEmp(6));
		System.out.println(empRepository.getEmployees());
		Employee e = new Employee();
		e.setId(1); e.setName("Cheav Sovannarith1");
		System.out.println("Insert -> "+ empRepository.updateEmp(e));
	}
}
