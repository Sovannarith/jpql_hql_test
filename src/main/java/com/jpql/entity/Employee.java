package com.jpql.entity;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name="selectEmployee", query = "select e from Employee e")
})
public class Employee {

    @Id @GeneratedValue
    private Integer id;

    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name.toString().toUpperCase() + '\'' +
                '}';
    }
}
