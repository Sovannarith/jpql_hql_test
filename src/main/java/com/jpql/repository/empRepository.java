package com.jpql.repository;

import com.jpql.entity.Employee;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.Document;

import javax.persistence.*;
import java.util.List;

@Repository
@Transactional
public class empRepository {

    @PersistenceContext
    EntityManager entityManager;
    Query query;

    public List<Employee> getEmployees(){
//        query = entityManager.createQuery("select e from Employee e");
        query = entityManager.createNamedQuery("selectEmployee");
        return query.getResultList();
    }
    public int deleteEmp(Integer id){
        query = entityManager.createNativeQuery("delete from Employee ee Where ee.id=:id");
        query.setParameter("id", id);
        int deletionQuery = query.executeUpdate();
        return deletionQuery;
    }
    public int insertEmp(Employee e){
        query = entityManager.createNativeQuery("INSERT INTO Employee(id, name) Values (:id, :name)");
        query.setParameter("id", e.getId());
        query.setParameter("name", e.getName());
        int insertQuery = query.executeUpdate();
        return insertQuery;
    }

    public int updateEmp(Employee e){
        query = entityManager.createQuery("update Employee set name = :name where id = :id");
        query.setParameter("id", e.getId());
        query.setParameter("name", e.getName());
        int updateStatus = query.executeUpdate();
        return updateStatus;
    }

}
